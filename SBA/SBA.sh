#Server Build Automation 
apt update
apt upgrade

apt intall nfs-kernel-server

#Docker
apt install -y apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list
apt update
apt install docker-ce docker-ce-cli containerd.io
systemctl enable docker

#Docker-Compose
curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

docker exec -it <idcontainer> gitlab-rake "gitlab:password:reset[root]"

#définir les partages NFS
#vim /etc/exports
service nfs-kernel-server reload

#liste des disques
fdisk -l

#Créer les partitions
#n: add new
#+1G pour partition de 1 Go
#w: écrire les modifications
fdisk /dev/sd*

#formater
mkfs -t ext4 /dev/sd**

mkdir /mnt/python
mkdir /mnt/jekyll
mkdir /mnt/tomcat
mkdir /mnt/backup.db

#montage éphémere des partitions
mount -t ext4 /dev/sd** <destination>

#enregister dans le fihier /etc/hosts l'IP du serveur LAMP

#montage permanant (autoriser une seule IP au partage)
echo "/mnt/python/     LAMP(rw,sync)" >> /etc/exports
echo "/mnt/tomcat/     LAMP(rw,sync)" >> /etc/exports
echo "/mnt/jekyll/     LAMP(ro,sync)" >> /etc/exports
echo "/mnt/backup.db/     LAMP(rw,sync)" >> /etc/exports

#efface toutes les régles de partages
exportfs -ua
#applique les régles de partage
exportfs -a
#voir les partages
showmount -e

#Jekyll
#Télécharger l'image
#docker pull jekyll/builder
#
#Construction d'un nouveau projet
#docker run --rm --volume="$PWD:/srv/jekyll:Z" -it jekyll/builder:$JEKYLL_VERSION jekyll new .
#
#Définition de la version de Jekyll utiliser || Compilation du contenu du répertoire courant (génére les fichiers du site)
#
#docker run --rm --volume="${PWD}:/srv/jekyll:Z" -it jekyll/builder:3.8 jekyll build

#Nouveau projet (génére les fichiers de travail compatible avec la version de compilation)
docker run -ti --rm -v ${PWD}:/srv/jekyll -e JEKYLL_ROOTLESS=1 docker.io/jekyll/builder jekyll new .

#Compilation du projet
docker run -ti --rm -v ${PWD}:/srv/jekyll -e JEKYLL_ROOTLESS=1 docker.io/jekyll/builder jekyll build

#Pour relier un runner au Gitlab, executer la commande suivante:
docker exec -it <id conteneur> gitlab-runner register

#https://eazytraining.fr/integration-continue-avec-gitlab-ce-et-jenkins/