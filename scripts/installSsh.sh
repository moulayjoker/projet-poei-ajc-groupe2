#! /bin/bash
#Vérifie si SSH est déja installé !
if [ -d /etc/ssh/ ]; then
	echo -n "SSH semble déjà installé. Tapez entrer pour continuer"
	read enter
else
	apt install openssh-server
	echo "SSH installé"
fi

#Boucle pour demander le choix de l'option choisi
while [ 1 -eq 1 ];
do
	echo -n "Activé Root pour SSH ? y pour oui / n pour non: ";
	read yn;
	if [[ $yn == y* ]]; then echo "PermitRootLogin yes" >> /etc/ssh/sshd_config ; echo "Root activé pour le SSH" ; exit; fi
	if [[ $yn == n* ]]; then exit; fi
done

#Redémarrer le demon
echo "Redemarrage du demon"
/etc/init.d/ssh restart

echo "SSH est opérationnel"