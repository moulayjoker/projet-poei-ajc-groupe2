|
|__addHostA2.sh <nom_domaine>	//ajoute un virtualhost à apache
|
|__installSsh.sh		//installe un serveur ssh et active l'acces root
|
|__newFstab.sh		//Ajout de partition avec menu
|  |
|  |__addFstab.sh <disque_montage> <point_montage> <type> //montage partition ou partage
|  //Code à revoir, comporte des erreurs
|
|__script_dns.sh
|  |
|  |__installBind.sh	//Installe Bind (serveur DNS)
|  |
|  |__addZone.sh		//Assistant d'ajout de zone DNS
|  |  |
|  |  |__addZoneBind.sh		//Enregistre la nouvelle zone dans le fichier de config et génére la zone
|  |  |  |
|  |  |  |__genZoneBind.sh <nom_zone>	//génére le fichier zone enregistré
|
|__installPaquet.sh <paquet1> <paquet2> ... <paquetN>
|
|__alias.sh		//ajoute des alias utiles à ~/.bash_aliases. Sera mis à jour au fur et à mesure
|
|__toolsBox		//installe divers outils utiles. Utilise le script installPaquet.sh