#! /bin/bash
echo "\$TTL	86400
@	IN	SOA	$1. root.$1. (
			      1		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			  86400 )	; Negative Cache TTL
;
@	IN	NS	$1." > "/etc/bind/db.$1"
echo La configuration de la zone \"$1\" a bien été créer sous le nom \"db.$1\"
cat "/etc/bind/db.$1"
echo "Tapez Entrer pour continuer"
read enter
