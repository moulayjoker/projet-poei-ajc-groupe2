#! /bin/bash
#Copie le fichier de config et écrase l'ancien fichier s'il existe
cp -rf /etc/bind/named.conf.local /etc/bind/named.conf.local.old
echo "nom de la zone"
read zone
echo "zone \"$zone\" {
type master;
file \"/etc/bind/$zone\";
};">> /etc/bind/named.conf.local
cat /etc/bind/named.conf.local
echo "Zone $zone ajouté ... Tapez Entrer"
read enter
echo "Enregistrement des parametres de la zone"
./genZoneBind.sh $zone
systemctl restart bind9
