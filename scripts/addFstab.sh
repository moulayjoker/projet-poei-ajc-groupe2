#! /bin/bash
mnt="/mnt/$2"
if [ -d "$mnt" ]; then
	if [ "$(ls -A $mnt)" ]; then
		echo "le répertoire \"$mnt\" non vide. Montage impossible !"; exit;
	fi
else
	echo "Le répertoire \"$mnt\" n'existe pas ! "
	while [ 1 -eq 1 ];
	do
		echo -n "Créer le point de montage $mnt ? y pour oui / n pour non: ";
		read yn;
		if [[ $yn == y* ]]; then mkdir "$2" ; echo "Point de montage crée" ; break; fi
		if [[ $yn == n* ]]; then echo  "Montage annulé, répertoire manquant" ; exit; fi
	done
fi
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
echo "$1	$mnt	$3	defaults	0	0" >> /etc/fstab
cat /etc/fstab
