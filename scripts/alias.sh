#! /bin/bash
#Permet de lister les fichiers et leurs attributs en couleurs et de manière chronologique
alias ll='ls -laA --color=auto'
