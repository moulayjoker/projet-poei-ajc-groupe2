#! /bin/bash
echo -n "Entrez le nom du montage: "
read src
echo -n "Entrez le nom du point de montage: "
read dest
echo -n "Type de montage: "
read t
<<stop
if [ -d "$src" ]; then
	./addFstab "$src" "$dest" "$t"
else echo -n "Source introuvable !"; touch enter; echo ""; exit
fi
stop

#Controle de la source mis en commentaire
./addFstab.sh "$src" "$dest" "$t"
#à supprimer quand le script de controle sera corrigé

while [ 1 -eq 1 ];
do
	echo -n "Voulez vous faire le montage maintenant ? y pour oui / n pour non: ";
	read yn;
	if [[ $yn == y* ]]; then mount -t "$t" "$src" "$dest"; echo -n "Montage fait"; read enter; break; fi
	if [[ $yn == n* ]]; then echo -n "Le montage se fera au redémarrage du systeme"; read enter; break; fi
done
df
