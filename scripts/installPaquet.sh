#! /bin/bash
#récupére la liste des arguments et lance leurs installation un par un
for deb in $* ; do
	#Vérifie si le paquet est déja installé !
	if [ -d "/etc/$deb/" ] ; then 
		echo -n "$deb semble déjà installé. Tapez entrer pour continuer" ; read enter ;
	else echo -n "Installation de $deb" ; read enter ; apt install "$deb" ; echo "$deb installé" ;
	fi
done
