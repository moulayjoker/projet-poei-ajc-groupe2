#! /bin/bash
site="/var/www/$1/public"
if [ ! -d "$site" ]; then
	mkdir -p "/var/www/$1/public"; echo "Le répertoire $site à été créé"
else echo "Le répertoire$site  existe "
fi
echo "<VirtualHost *:80>
	#Le site est ouvert à toutes les IPs
    ServerName $1
	#Ajout de l'alias en www
    ServerAlias $1 www.$1
    ServerAdmin webmaster@$1
	#Adresse générique, à personnalisé au besoin
    DocumentRoot $site
	#Options pour séparer les logs, activables
	CustomLog ${APACHE_LOG_DIR}/$1-access.log combined
    ErrorLog ${APACHE_LOG_DIR}/$1-error.log

	#Toutes les options sont activés
    <Directory $site>
        Options All
		#Empéche que les htaccess modifient la configuration d'apache (sécurité)
        AllowOverride None
    </Directory>
</VirtualHost>" > "/etc/apache2/sites-available/01-$1.conf"

echo "Votre nouveau host $1 est généré."

while [ 1 -eq 1 ];
do
	echo -n "Désactiver le site par défaut ? y pour oui / n pour non: ";
	read yn;
	if [[ $yn == y* ]]; then a2dissite 000-default.conf; echo -n "Site par défaut désactivé"; read enter; break; fi
	if [[ $yn == n* ]]; then echo -n "Nous n'avons pas modifier votre configuration !"; read enter; break; fi
done

while [ 1 -eq 1 ];
do
	echo -n "Activer le nouveau site $1 ? y pour oui / n pour non: ";
	read yn;
	if [[ $yn == y* ]]; then a2ensite "01-$1.conf"; echo -n "Site $1 activé"; read enter; break; fi
	if [[ $yn == n* ]]; then echo -n "Nous n'avons pas démarré le site $1 !"; read enter; break; fi
done
systemctl reload apache2