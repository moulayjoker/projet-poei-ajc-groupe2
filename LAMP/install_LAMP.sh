apt update
apt upgrade
apt install apache2 nfs-common

#modifier le fichier hosts pour définir les hostname à certaines IP

#Docker
apt install -y apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list
apt update
apt install docker-ce docker-ce-cli containerd.io
systemctl enable docker

#Docker-Compose
curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

mkdir -p /var/www/python/public
mkdir -p /var/www/jekyll/public
mkdir -p /var/www/tomcat/public
mkdir -p /var/www/phpmyadmin/public
mkdir -p /var/scripts/

docker build -t web-python-ajc:2 .
docker run -d -p 8000:8000 --name web-python -v /var/www/python/public/:/app/www/ web-python-ajc:2

docker-compose up
#Jekyll
apt-get install ruby-full build-essential
gem install jekyll bundler
jekyll new /var/www/jekyll/public

#Construction des pages
jekyll build

# -w: watch relance un build quand le répertoire est modifié automatiquement
# -s: repertoire source
# -d: repertoire destination
# -P: port d'écoute
# bundle exec jekyll serve: utilise la version défini de gem
jekyll serve -w -s /var/www/jekyll/public

#activation du revers proxy et de proxy pass
a2enmod proxy proxy_http

#ecrire les configs apache revers-proxy et pass-proxy
#activation des configurations
a2ensite 001-jekyll.conf
a2ensite 001-python.web.conf

systemctl reload apache2

#liste des partages disponibles sur un serveur
showmount -e <ip_serveur>