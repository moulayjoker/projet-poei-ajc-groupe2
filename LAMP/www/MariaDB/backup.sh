#! /bin/bash
#00 * * * * /var/www/MariaDB/backup.sh

backup="/var/www/MariaDB/backup"
mkdir ${backup}

#on suppose que le dossier est vide
mount -t nfs SMA:/mnt/backup.db/ ${backup}

archive=$(date "%Y%m%d%H")

tar -cvzf "${backup}/MySQL-$archive.taz" "${backup}/../db_data/"

umount ${backup}

rm -r ${backup}